{
  "targets": [
    {
      "target_name": "ftp_sync",
      "sources": [ "ftp_sync.cpp" ],
      "include_dirs": [ 'usr/include/nodejs/src',
            'usr/include/nodejs/deps/uv',
            'usr/include/nodejs/deps/v8'],
      "link_settings": {
                        'libraries': ['-lcurl -L/usr/lib/libv8.so'],
                        'library_dirs': ['/usr/lib/'],
                       },
      'cflags!': [ '-fno-exceptions' ],
      'cflags_cc!': [ '-fno-exceptions' ],
      'conditions': [
        ['OS=="mac"', {
          'xcode_settings': {
            'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'
          }
        }],
        ['OS=="win"', {
          'xcode_settings': {
            'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'
          }
        }],
        ['OS !="win"', {
          'xcode_settings': {
            'GCC_ENABLE_CPP_EXCEPTIONS': 'YES'
          }
        }],
      ]
    }
  ]
}
