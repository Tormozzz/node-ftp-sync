#include <node.h>
#include <node_buffer.h>
#include <v8.h>
#include <iostream>
#include <string>

#include <curl/curl.h>

using namespace v8;
using namespace std;

struct FtpFile {
  const char *filename;
  FILE *stream;
};

static size_t my_fwrite(void *buffer, size_t size, size_t nmemb, void *stream)
{
  struct FtpFile *out=(struct FtpFile *)stream;
  if(out && !out->stream) {
    out->stream=fopen(out->filename, "wb");
    if(!out->stream)
      return -1;
  }
  return fwrite(buffer, size, nmemb, out->stream);
}

Handle<Value> connect(const Arguments& args)
{
    HandleScope scope;
    Handle<Object> ret = Object::New();

    Handle<External> field = Handle<External>::Cast(args.Holder()->GetHiddenValue(String::NewSymbol("curl")));

    CURL* curl = (CURL *)field->Value();
    CURLcode result;
    if (curl)
    {
        bool ready = false;
        while (!ready)
        {
            try
            {
                String::AsciiValue host(args[0]->ToString());
                String::AsciiValue user(args[1]->ToString());
                String::AsciiValue pass(args[2]->ToString());
                string str = *user;
                str += ":";
                str += *pass;

                curl_easy_setopt(curl, CURLOPT_URL, *host);
                curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
                curl_easy_setopt(curl, CURLOPT_USERPWD, str.c_str());
                result = curl_easy_perform(curl);
                if (CURLE_OK == result)
                {
                    args.Holder()->Set(String::NewSymbol("host"), String::New(*host));
                    args.Holder()->Set(String::NewSymbol("user"), String::New(*user));
                    args.Holder()->Set(String::NewSymbol("pass"), String::New(*pass));
                    args.Holder()->Set(String::NewSymbol("connected"), Boolean::New(1));
                }
                else
                {
                    ret->Set(String::NewSymbol("error"), String::New(curl_easy_strerror(result)));
                }
                ready = true;
            }
            catch (Exception e)
            {
                curl = curl_easy_init();
                args.Holder()->SetHiddenValue(String::NewSymbol("curl"), External::New((void*)curl));
                ready = false;
            }
        }
    }
    else
    {
        ret->Set(String::NewSymbol("error"), String::New("libcurl is not initialized"));
    }

    return scope.Close(ret);
}

Handle<Value> reconnect(const Arguments& args)
{
    HandleScope scope;
    Handle<Object> ret = Object::New();

    Handle<External> field = Handle<External>::Cast(args.Holder()->GetHiddenValue(String::NewSymbol("curl")));

    CURL* curl = (CURL *)field->Value();
    CURLcode result;
    if (curl)
    {
        bool ready = false;
        while (!ready)
        {
            try
            {
                String::AsciiValue host(args.Holder()->Get(String::NewSymbol("host"))->ToString());
                String::AsciiValue user(args.Holder()->Get(String::NewSymbol("user"))->ToString());
                String::AsciiValue pass(args.Holder()->Get(String::NewSymbol("pass"))->ToString());
                string str = *user;
                str += ":";
                str += *pass;

                curl_easy_setopt(curl, CURLOPT_URL, *host);
                curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
                curl_easy_setopt(curl, CURLOPT_USERPWD, str.c_str());
                result = curl_easy_perform(curl);
                if (CURLE_OK == result)
                {
                    args.Holder()->Set(String::NewSymbol("host"), String::New(*host));
                    args.Holder()->Set(String::NewSymbol("user"), String::New(*user));
                    args.Holder()->Set(String::NewSymbol("pass"), String::New(*pass));
                    args.Holder()->Set(String::NewSymbol("connected"), Boolean::New(1));
                }
                else
                {
                    ret->Set(String::NewSymbol("error"), String::New(curl_easy_strerror(result)));
                }
                ready = true;
            }
            catch (Exception e)
            {
                curl = curl_easy_init();
                args.Holder()->SetHiddenValue(String::NewSymbol("curl"), External::New((void*)curl));
                reconnect(args);
                ready = false;
            }
        }
    }
    else
    {
        ret->Set(String::NewSymbol("error"), String::New("libcurl is not initialized"));
    }

    return scope.Close(ret);
}

Handle<Value> get(const Arguments& args)
{
    HandleScope scope;
    Handle<Object> ret = Object::New();
    CURLcode result;

    String::AsciiValue local(args[0]->ToString());
    String::AsciiValue remote(args[1]->ToString());
    String::AsciiValue host(args.Holder()->Get(String::NewSymbol("host"))->ToString());

    string fullPath = *host;
    fullPath += '/';
    fullPath += *remote;

    struct FtpFile ftpfile={
        *local,
        NULL
    };

    Handle<External> field = Handle<External>::Cast(args.Holder()->GetHiddenValue(String::NewSymbol("curl")));

    CURL* curl = (CURL *)field->Value();

    if (curl)
    {
        bool ready = false;
        while (!ready)
        {
            try
            {
                curl_easy_setopt(curl, CURLOPT_URL, fullPath.c_str());
                curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_fwrite);
                curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpfile);
                result = curl_easy_perform(curl);

                if(ftpfile.stream)
                    fclose(ftpfile.stream);

                if (CURLE_OK == result)
                {
                    args.Holder()->Set(String::NewSymbol("host"), String::New(*host));
                    args.Holder()->Set(String::NewSymbol("connected"), Boolean::New(1));
                }
                else
                {
                    ret->Set(String::NewSymbol("error"), String::New(curl_easy_strerror(result)));
                }
                ready = true;
            }
            catch (exception e)
            {
                curl = curl_easy_init();
                args.Holder()->SetHiddenValue(String::NewSymbol("curl"), External::New((void*)curl));
                reconnect(args);
                ready = false;
            }
        }
    }
    else
    {
        ret->Set(String::NewSymbol("error"), String::New("libcurl is not initialized"));
    }

    return scope.Close(ret);
}

Handle<Value> create(const Arguments& args)
{
    CURL * curl;
    HandleScope scope;
    Handle<Object> ret = Object::New();

    curl_global_init(CURL_GLOBAL_DEFAULT);

    curl = curl_easy_init();
    curl_easy_perform(curl);

    node::Buffer * buf;
    cout << "sizeof(CURL) = " << sizeof(CURL) << endl;
    cout << "curl = " << curl << endl;

    ret->SetHiddenValue(String::NewSymbol("curl"), External::New((void*)curl));
    ret->Set(String::NewSymbol("connected"), Boolean::New(0));
    ret->Set(String::NewSymbol("connect"), FunctionTemplate::New(connect)->GetFunction());
    ret->Set(String::NewSymbol("get"), FunctionTemplate::New(get)->GetFunction());

    return scope.Close(ret);
}

Handle<Value> clean(const Arguments& args)
{
    HandleScope scope;
    Handle<Object> ret = Object::New();

    Handle<External> field = Handle<External>::Cast(args.Holder()->GetHiddenValue(String::NewSymbol("curl")));

    CURL* curl = (CURL *)field->Value();

    if (curl)
    {
        curl_easy_cleanup(curl);
        curl_global_cleanup();
    }
    else
    {
        ret->Set(String::NewSymbol("error"), String::New("libcurl is not initialized"));
    }

    return scope.Close(ret);
}

void init(Handle<Object> target)
{
    target->Set(String::NewSymbol("create"), FunctionTemplate::New(create)->GetFunction());
}
NODE_MODULE(ftp_sync, init);
